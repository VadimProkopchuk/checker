﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Scheduler.Twitter
{
    public class TwitterScheduler
    {
        public static void Start()
        {
            var scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();

            var job = JobBuilder.Create<TwitterCheckerJob>().Build();

            var trigger = TriggerBuilder.Create()  
                .WithIdentity("TwitterChecker", "Group1")     
                .StartNow()                            
                .WithSimpleSchedule(x => x            
                    .WithIntervalInMinutes(5)          
                    .RepeatForever())                   
                .Build();                               

            scheduler.ScheduleJob(job, trigger);
        }
    }
}