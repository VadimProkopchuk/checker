﻿using Quartz;
using System.Configuration;
using WebApplication1.Helpers;
using WebApplication1.Twitter;

namespace WebApplication1.Scheduler.Twitter
{
    public class TwitterCheckerJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            var сonsumerKey = ConfigurationManager.AppSettings["OAuthConsumerKey"].ToString();
            var consumerSecret = ConfigurationManager.AppSettings["OAuthConsumerSecret"].ToString();
            var userName = ConfigurationManager.AppSettings["UserName"].ToString();
            var twitterProvider = new TwitterProvider(сonsumerKey, consumerSecret);

            AsyncHelpers.RunSync(() => twitterProvider.AuthAsync());
            var lastTwit = AsyncHelpers.RunSync(() => twitterProvider.GetLastTwitAsync(userName));

            // PROCESS twit info

        }
    }
}