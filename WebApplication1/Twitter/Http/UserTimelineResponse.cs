﻿using Newtonsoft.Json;
using System;

namespace WebApplication1.Twitter.Entities
{
    public class UserTimelineResponse
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }
    }
}