﻿using Newtonsoft.Json;

namespace WebApplication1.Twitter.Entities
{
    public class AccessTokenResponse
    {
        [JsonProperty("token_type")]
        public string Type { get; set; }

        [JsonProperty("access_token")]
        public string Token { get; set; }
    }
}