﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using WebApplication1.Twitter.Entities;

namespace WebApplication1.Twitter
{
    public class TwitterProvider
    {
        private const string oAuthUrl = "https://api.twitter.com/oauth2/token";
        private const string userTimelineUrl = "https://api.twitter.com/1.1/statuses/user_timeline.json";

        private readonly string oAuthConsumerKey;
        private readonly string oAuthConsumerSecret;

        private string token;
        private string tokenType;

        public TwitterProvider(string consumerKey, string consumerSecret)
        {
            oAuthConsumerKey = consumerKey;
            oAuthConsumerSecret = consumerSecret;
        }

        public async Task AuthAsync()
        {
            using (var httpClient = new HttpClient())
            {
                var request = CreateAuthenticationRequest();
                var response = await httpClient.SendAsync(request);
                var json = await response.Content.ReadAsStringAsync();
                var accessTokenResponce = JsonConvert.DeserializeObject<AccessTokenResponse>(json);

                token = accessTokenResponce.Token;
                tokenType = accessTokenResponce.Type;
            }
        }

        public async Task<Twit> GetLastTwitAsync(string username) => (await GetTweetsAsync(username, 1)).FirstOrDefault();

        public async Task<IEnumerable<Twit>> GetTweetsAsync(string username, int count)
        {
            if (!HasAuth)
            {
                await AuthAsync();
            }

            var requestQuery = $"{userTimelineUrl}?count={count}&screen_name={username}&trim_user=1&exclude_replies=1";
            var request = new HttpRequestMessage(HttpMethod.Get, requestQuery);

            request.Headers.Add("Authorization", $"{tokenType} {token}");

            using (var httpClient = new HttpClient())
            {
                var response = await httpClient.SendAsync(request);
                var json = await response.Content.ReadAsStringAsync();
                var userTimelineResponse = JsonConvert.DeserializeObject<UserTimelineResponse[]>(json);

                return userTimelineResponse.Select(x => new Twit { Text = x.Text, Id = x.Id });
            }
        }

        public bool HasAuth => !String.IsNullOrEmpty(token) && !String.IsNullOrEmpty(tokenType);

        private string GetCustomerInfo()
        {
            var encoder = new UTF8Encoding();
            var customerInfo = $"{oAuthConsumerKey}:{oAuthConsumerSecret}";

            return Convert.ToBase64String(encoder.GetBytes(customerInfo));
        }

        private HttpRequestMessage CreateAuthenticationRequest()
        {
            var request = new HttpRequestMessage(HttpMethod.Post, oAuthUrl);
            var customerInfo = GetCustomerInfo();

            request.Headers.Add("Authorization", $"Basic {customerInfo}");
            request.Content = new StringContent("grant_type=client_credentials", Encoding.UTF8, "application/x-www-form-urlencoded");

            return request;
        }
    }
}